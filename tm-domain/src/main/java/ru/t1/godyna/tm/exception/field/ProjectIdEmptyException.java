package ru.t1.godyna.tm.exception.field;

public final class ProjectIdEmptyException extends AbsractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}
